Порядок запуска:

1) kml_to_json_parser.js
Берем kml файл размером 1.2 гб и парсим на координаты.
На выходе получаем объекты с информацией: координаты, имя, postcode1-4, shape_length (для доп проверки на подлиность)

2) xlsx_to_json_converter.js
Конвертируем xlsx файл в жсон, на выходе получаем 4 json файла (4 страницы/листа в документе xlsx).
Получаем около 40810 обьектов для поиска координат (города, округи, дачи, села, области итд).

3) json_inserter.js
каждый из 40810 объектов я прогоняю по всем json файлам из 1го пункта для поиска координат.
по итогу записываю все в файлы output_Admin(1-4)_sheet.json


4)json_to_xlsx.js
обратный парсер в xlsx

Необходимые пакеты:
npm install xlsx-to-json
npm install line-reader
npm install cheerio
npm install json2xls
