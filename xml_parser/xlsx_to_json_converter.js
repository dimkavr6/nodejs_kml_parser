const xlsxj = require("xlsx-to-json");
const xlsxFileName = 'ukr_adminboundaries_tabulardata.xlsx';
const sheetNamesOfXlsxFile = ['Admin0','Admin1','Admin2','Admin3','Admin4'];


const startConvertingXlsxFile = async() => {
  for (var i = 0; i < sheetNamesOfXlsxFile.length; i++) {
    await converterXlsxFile(sheetNamesOfXlsxFile[i]).then((resolvedVar) => {
      console.log(resolvedVar);
    });
  }
};

const converterXlsxFile = (sheetName) => {
  return new Promise((resolve, reject) => {
    xlsxj({
      input: xlsxFileName,
      output: 'result_of_parsing_xlsx/' + sheetName + '_sheet.json',
      sheet: sheetName
    },(err, result) => {
      if(err){
        console.error(err);
      } else {
        resolve('XLSXconverter: sheet - ' + sheetName + ' of document ' + xlsxFileName + ' succesfully done.');
      }
    });
  });
};

startConvertingXlsxFile();
