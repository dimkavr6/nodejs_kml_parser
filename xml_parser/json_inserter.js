// TODO: рефакторинг свича уровней
// TODO: заюзать indexof для проверок
//  node --max-old-space-size=8592 json_inserter

const fs = require('fs');
//folders with results of parsing kml and xlsx
const path_kml = 'result_of_parsing_kml';
const path_xlsx = 'result_of_parsing_xlsx';
var globalFinalObject = [];
var localObjectCounter = 0;
var fouder_debug = 0;
var programtime;
let notFounded = 0;

//функция, которая даст массив со всеми именами файлов, по которым мы будем в дальнейшем выполнять поиск
//добавлена сортировка файлов
const getAllFilesNameFromDirectory = (path) => {
  var arrayOfFileNames = [];
  fs.readdirSync(path).forEach(filename => {
    arrayOfFileNames.push(filename);
  });
  //sorting files
  arrayOfFileNames.sort(function(a, b) {
    return a.localeCompare(b, undefined, {
        numeric: true,
        sensitivity: 'base'
    });
  });
  return arrayOfFileNames;
};

const createArrayFromJSONxlsx = async(xlsxfilename) => {
  return new Promise((resolve, reject) => {
    var infoFromJsonXLSX = JSON.parse(fs.readFileSync(path_xlsx + '/' + xlsxfilename, 'utf8'));
    resolve(infoFromJsonXLSX);
  });
  }

//получить номер Admin level PostCode for search_postcode
const getAdminLevel = (filename) => {
  return parseInt(filename.replace('Admin','').replace('_sheet.json',''));
};

const xlsxFilesName = getAllFilesNameFromDirectory(path_xlsx);
const kmlFilesName = getAllFilesNameFromDirectory(path_kml);

// === START HERE ==== //
const xlsxStartRead = async() => {
  for (var i = 0; i < xlsxFilesName.length; i++) {
    await createArrayFromJSONxlsx(xlsxFilesName[i]).then(async(result) => {
      console.log('Create [array] from JSON file : ' + xlsxFilesName[i]);
      await startSearchCoordinates(xlsxFilesName[i], result, getAdminLevel(xlsxFilesName[i])).then((result) => {
        console.log(result);
      });
    });
  }
};

const startSearchCoordinates = (xlsxFileName, arrayWithObjects, adminPcodeLevel) => {
  return new Promise(async(resolve, reject) => {
    for (var i = 0; i < arrayWithObjects.length; i++) {
      console.log('Начинаю поиск координат для: (' + (i+1) + ') ' + arrayWithObjects[i]['admin'+ adminPcodeLevel +'Name_ru'] + ' (Lvl: ' + adminPcodeLevel + ', PostCode: ' + arrayWithObjects[i]['admin'+ adminPcodeLevel +'Pcode'] + ')');
      await startSearchForCurrentObject(arrayWithObjects[i], adminPcodeLevel).then((result) => {
        console.log(result);
      });
    }
      fs.writeFileSync(path_xlsx + '/output_' + xlsxFileName, JSON.stringify(globalFinalObject));
      globalFinalObject = [];
      console.log('создан файл: output_' + xlsxFileName);
      resolve('Завершил проверку по XLSX файлу, переходим к следующему.');
      console.log('// DEBUG: info - ' + fouder_debug);
      console.log('// DEBUG: time - ' + (new Date() - programtime));
      console.log('// DEBUG: not founded for: ' + notFounded);
  });
};

const startSearchForCurrentObject = (singleObjectFromArray, level) => {
  return new Promise(async(resolve, reject) => {
    for (var i = 0; i < kmlFilesName.length; i++) {
      //var infoFromJsonKML = JSON.parse(fs.readFileSync(path_kml + '/' + kmlFilesName[i], 'utf8'));
      var infoFromJsonKML = require('./result_of_parsing_kml/'+kmlFilesName[i]+'');
      await searchEachKMLObject(infoFromJsonKML, singleObjectFromArray, level, kmlFilesName[i]).then((result) => {
      });
    }
    localObjectCounter = 0;
    resolve('Перехожу к следующему...');
  });
};

const compareCoordinates = (level, objectInfoFromJsonKML, singleObjectFromArray, iterator, kmlFileName) => {
  if(level == 0 || level == 1){
    console.log('!!!!! Координаты найдены в объекте номер: ' + iterator + ' в файле : ( ' + kmlFileName + ' )');
    singleObjectFromArray['pointCoord'] = objectInfoFromJsonKML['pointCoord'];
    singleObjectFromArray['linestringCoord'] = objectInfoFromJsonKML['linestringCoord'];
    singleObjectFromArray['linearRingCoord'] = objectInfoFromJsonKML['linearRingCoord'];
    globalFinalObject.push(singleObjectFromArray);
    fouder_debug++;
  } else {
    console.log('!!!!! Координаты найдены в объекте номер: ' + iterator + ' в файле : ( ' + kmlFileName + ' )');
    if(!localObjectCounter){
      singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = objectInfoFromJsonKML['pointCoord'];
      singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = objectInfoFromJsonKML['linestringCoord'];
      singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = objectInfoFromJsonKML['linearRingCoord'];
      localObjectCounter++;
    } else {
      singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = objectInfoFromJsonKML['pointCoord'];
      singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = objectInfoFromJsonKML['linestringCoord'];
      singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = objectInfoFromJsonKML['linearRingCoord'];
      fouder_debug++;
      globalFinalObject.push(singleObjectFromArray);
    }
  }

  return;
};

const searchEachKMLObject = (infoFromJsonKML, singleObjectFromArray, level, kmlFileName) => {
  return new Promise((resolve, reject) => {
    for (var i = 1; i <= Object.keys(infoFromJsonKML).length; i++) {
      switch (level) {
        case 0:
          if(
            infoFromJsonKML['Object0' + i]['ADM0_PCODE'] == singleObjectFromArray['admin0Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM1_PCODE'] == 'none' &&
            infoFromJsonKML['Object0' + i]['ADM2_PCODE'] == 'none' &&
            infoFromJsonKML['Object0' + i]['ADM3_PCODE'] == 'none' &&
            infoFromJsonKML['Object0' + i]['ADM4_PCODE'] == 'none'
          ){
            compareCoordinates(level, infoFromJsonKML['Object0' + i], singleObjectFromArray, i, kmlFileName);
            /*
            console.log('!!!!! Координаты найдены в объекте номер: ' + i + ' в файле : ( ' + kmlFileName + ' )');
            singleObjectFromArray['pointCoord'] = infoFromJsonKML['Object0' + i]['pointCoord'];
            singleObjectFromArray['linestringCoord'] = infoFromJsonKML['Object0' + i]['linestringCoord'];
            singleObjectFromArray['linearRingCoord'] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
            globalFinalObject.push(singleObjectFromArray);
            fouder_debug++;
            */
          }
          break;

        case 1:
          if(
            infoFromJsonKML['Object0' + i]['ADM0_PCODE'] == singleObjectFromArray['admin0Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM1_PCODE'] == singleObjectFromArray['admin1Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM2_PCODE'] == 'none' &&
            infoFromJsonKML['Object0' + i]['ADM3_PCODE'] == 'none' &&
            infoFromJsonKML['Object0' + i]['ADM4_PCODE'] == 'none'
          ){
            compareCoordinates(level, infoFromJsonKML['Object0' + i], singleObjectFromArray, i, kmlFileName);
            /*
            console.log('!!!!! Координаты найдены в объекте номер: ' + i + ' в файле : ( ' + kmlFileName + ' )');
            singleObjectFromArray['pointCoord'] = infoFromJsonKML['Object0' + i]['pointCoord'];
            singleObjectFromArray['linestringCoord'] = infoFromJsonKML['Object0' + i]['linestringCoord'];
            singleObjectFromArray['linearRingCoord'] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
            globalFinalObject.push(singleObjectFromArray);
            fouder_debug++;
            */
          }
          break;

        case 2:
          if(
            infoFromJsonKML['Object0' + i]['ADM0_PCODE'] == singleObjectFromArray['admin0Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM1_PCODE'] == singleObjectFromArray['admin1Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM2_PCODE'] == singleObjectFromArray['admin2Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM3_PCODE'] == 'none' &&
            infoFromJsonKML['Object0' + i]['ADM4_PCODE'] == 'none'
          ){
            compareCoordinates(level, infoFromJsonKML['Object0' + i], singleObjectFromArray, i, kmlFileName);
          /*
            console.log('!!!!! Координаты найдены в объекте номер: ' + i + ' в файле : ( ' + kmlFileName + ' )');
            if(!localObjectCounter){
              singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['pointCoord'];
              singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linestringCoord'];
              singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
              localObjectCounter++;
            } else {
              singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['pointCoord'];
              singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linestringCoord'];
              singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
              fouder_debug++;
              globalFinalObject.push(singleObjectFromArray);
            }
            */
          }
          break;

        case 3:
          if(
            infoFromJsonKML['Object0' + i]['ADM0_PCODE'] == singleObjectFromArray['admin0Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM1_PCODE'] == singleObjectFromArray['admin1Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM2_PCODE'] == singleObjectFromArray['admin2Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM3_PCODE'] == singleObjectFromArray['admin3Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM4_PCODE'] == 'none'
          ){
            compareCoordinates(level, infoFromJsonKML['Object0' + i], singleObjectFromArray, i, kmlFileName);
          /*
            console.log('!!!!! Координаты найдены в объекте номер: ' + i + ' в файле : ( ' + kmlFileName + ' )');
            if(!localObjectCounter){
              singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['pointCoord'];
              singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linestringCoord'];
              singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
              localObjectCounter++;
            } else {
              singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['pointCoord'];
              singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linestringCoord'];
              singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
              fouder_debug++;
              globalFinalObject.push(singleObjectFromArray);
            }
            */
          }
          break;

        case 4:
          if(
            infoFromJsonKML['Object0' + i]['ADM0_PCODE'] == singleObjectFromArray['admin0Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM1_PCODE'] == singleObjectFromArray['admin1Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM2_PCODE'] == singleObjectFromArray['admin2Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM3_PCODE'] == singleObjectFromArray['admin3Pcode'] &&
            infoFromJsonKML['Object0' + i]['ADM4_PCODE'] == singleObjectFromArray['admin4Pcode']
          ){
            compareCoordinates(level, infoFromJsonKML['Object0' + i], singleObjectFromArray, i, kmlFileName);
          /*
            console.log('!!!!! Координаты найдены в объекте номер: ' + i + ' в файле : ( ' + kmlFileName + ' )');
            if(!localObjectCounter){
              singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['pointCoord'];
              singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linestringCoord'];
              singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
              localObjectCounter++;
            } else {
              singleObjectFromArray['pointCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['pointCoord'];
              singleObjectFromArray['linestringCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linestringCoord'];
              singleObjectFromArray['linearRingCoord' + (localObjectCounter + 1)] = infoFromJsonKML['Object0' + i]['linearRingCoord'];
              fouder_debug++;
              globalFinalObject.push(singleObjectFromArray);
            }
            */
          }
          break;

      }

    }
    resolve();

  });
};

//запускаем скрипт
programtime = new Date();
xlsxStartRead();
