//для проверки, или обратно перегнать в xlsx
const json2xls = require('json2xls');
const fs = require('fs');

var jsonFile = JSON.parse(fs.readFileSync('result_of_parsing_xlsx/output_Admin1_sheet.json', 'utf8'));
var xls = json2xls(jsonFile);
fs.writeFileSync('result_of_parsing_xlsx/output_Admin1_sheet.xlsx', xls, 'binary');
