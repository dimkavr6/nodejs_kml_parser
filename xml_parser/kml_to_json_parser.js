const lineReader = require('line-reader');
const fs = require('fs');
const cheerio = require('cheerio');

const kmlFileName = 'doc.kml';
var tmpStringOfCurrentTag = '';
var isCopyAllObject = false;
var counterForObject = 0;
var globalObjectCounter = 0;
var fileNameCounter = 0;
var scriptStartDate = new Date();
var obj = {};
let tmpSpaceFix = '';
//var debugCounter = 0;

//new update функция для конвертации строки с координатами в нужный формат для http://geojson.io
const convertToGeoJSON = (string, type) => {
  let finalResult = [];

  if(type == 'Point'){
    string = string.split(',');
    for (var i = 0; i < string.length; i++) {
      string[i] = parseFloat(string[i]);
    }
    finalResult = string;
  }
  if(type == 'Polygon' || type == 'Polyline'){
    string = string.split(' ');
    for (var i = 0; i < string.length; i++) {
      string[i] = string[i].split(',');
      for (var y = 0; y < string[i].length; y++) {
        string[i][y] = parseFloat(string[i][y]);
      }
    }
    if(type == 'Polygon'){
      //let finallArray = [];
      //finallArray.push(string);
      //finalResult = finallArray;
      finalResult = string;
    } else {
      finalResult = string;
    }
  }

  return finalResult;
};



//читаем по одной строке с помощью line-reader в цикле
//вся нужная нам информация находится в теге <Placemark>, остальное нам не интересно

//смысл следующий: пишем в tmpStringOfCurrentTag все что находится в теге <Placemark></Placemark>
//далее парсим эту строку с помощью cheerio (html parser), который без проблем справляется с данной задачей
lineReader.eachLine(kmlFileName, function(line, last) {
  //нашли начало тега Placemark
  if(line.search('<Placemark id=') != -1){
    //разрешаем запиcать данную строку в tmpStringOfCurrentTag
    isCopyAllObject = true;
  }
  //нашли конец тега, у нас уже есть вся информация
  if(line.search('</Placemark>') != -1){
    tmpStringOfCurrentTag += line;  //записываем данную строку с закрывающем тегом в tmpStringOfCurrentTag
    isCopyAllObject = false;  //reset boolean var
    /*
    counterForObject++;
    console.log(counterForObject + ' - object succesfull created');
    obj['Object0' + counterForObject] = new Object();
    */
    //==========================parsing with cheerio==========================================
    //var $ = cheerio.load(JSON.stringify(tmpStringOfCurrentTag));
    //try to load string
    let $ = cheerio.load(tmpStringOfCurrentTag);
    //BASE INFO
    //var coordinates = $('coordinates').text();
    //var Shape_Le_1 = $('td').filter(function() { return $(this).text().trim() === 'Shape_Le_1'; }).next().text()?$('td').filter(function() { return $(this).text().trim() === 'Shape_Le_1'; }).next().text():'none';
    //POST CODES
    var ADM0_PCODE = $('td').filter(function() { return $(this).text().trim() === 'ADM0_PCODE'; }).next().text()?$('td').filter(function() { return $(this).text().trim() === 'ADM0_PCODE'; }).next().text():'none';
    var ADM1_PCODE = $('td').filter(function() { return $(this).text().trim() === 'ADM1_PCODE'; }).next().text()?$('td').filter(function() { return $(this).text().trim() === 'ADM1_PCODE'; }).next().text():'none';
    var ADM2_PCODE = $('td').filter(function() { return $(this).text().trim() === 'ADM2_PCODE'; }).next().text()?$('td').filter(function() { return $(this).text().trim() === 'ADM2_PCODE'; }).next().text():'none';
    var ADM3_PCODE = $('td').filter(function() { return $(this).text().trim() === 'ADM3_PCODE'; }).next().text()?$('td').filter(function() { return $(this).text().trim() === 'ADM3_PCODE'; }).next().text():'none';
    var ADM4_PCODE = $('td').filter(function() { return $(this).text().trim() === 'ADM4_PCODE'; }).next().text()?$('td').filter(function() { return $(this).text().trim() === 'ADM4_PCODE'; }).next().text():'none';

    //если все 5 postcode отсутствуют, то нам такая информация не нужна
    if(!(ADM0_PCODE == 'none' && ADM1_PCODE == 'none' && ADM2_PCODE == 'none' && ADM3_PCODE == 'none' && ADM4_PCODE == 'none')){

      var name = $('name').text();
      var Shape_Leng = $('td').filter(function() { return $(this).text().trim() === 'Shape_Leng'; }).next().text()?$('td').filter(function() { return $(this).text().trim() === 'Shape_Leng'; }).next().text():'none';

      //1:Point (only one coordinates) == point
      var pointCoord;
      if($('Placemark').find('coordinates').parent()[0].name == 'point'){
        pointCoord = 'space_delete' + $('Placemark').find('coordinates').text();
        pointCoord = pointCoord.replace('space_delete ','');  //lifehack to delete first space
        pointCoord = convertToGeoJSON(pointCoord, 'Point');
      } else {
        pointCoord = 'none';
      }
      //2:LineString (only one coordinates) == linestring
      var linestringCoord;
      if($('Placemark').find('coordinates').parent()[0].name == 'linestring'){
        linestringCoord = 'space_delete' + $('Placemark').find('coordinates').text();
        linestringCoord = linestringCoord.replace('space_delete ','');  //lifehack to delete first space
        linestringCoord = convertToGeoJSON(linestringCoord, 'Polyline');
      } else {
        linestringCoord = 'none';
      }
      //3:LinearRing (one and more coordinates) == linearring
      var linearRingCoord;
      if($('Placemark').find('coordinates').parent()[0].name == 'linearring'){
        linearRingCoord = [];
        //loop for search and save all coordinates
        $('Placemark').find('coordinates').each(function(i, element){
          tmpSpaceFix = 'space_delete' + $(this).text();
          tmpSpaceFix = tmpSpaceFix.replace('space_delete ','');  //lifehack to delete first space
          tmpSpaceFix = convertToGeoJSON(tmpSpaceFix, 'Polygon');
          linearRingCoord.push(tmpSpaceFix);
        });
      } else {
        linearRingCoord = 'none';
      }

      //debugCounter++;
      counterForObject++;
      globalObjectCounter++;
      console.log(globalObjectCounter + ' - (' + counterForObject + ') - object succesfull created');
      obj['Object0' + counterForObject] = new Object();
      //записываем всю информацию в объект
      obj['Object0' + counterForObject]['name'] = name;
      obj['Object0' + counterForObject]['Shape_Leng'] = Shape_Leng;
      //obj['Object0' + counterForObject]['Shape_Le_1'] = Shape_Le_1;
      //obj['Object0' + counterForObject]['coordinates'] = coordinates;
      obj['Object0' + counterForObject]['ADM0_PCODE'] = ADM0_PCODE;
      obj['Object0' + counterForObject]['ADM1_PCODE'] = ADM1_PCODE;
      obj['Object0' + counterForObject]['ADM2_PCODE'] = ADM2_PCODE;
      obj['Object0' + counterForObject]['ADM3_PCODE'] = ADM3_PCODE;
      obj['Object0' + counterForObject]['ADM4_PCODE'] = ADM4_PCODE;
      obj['Object0' + counterForObject]['pointCoord'] = pointCoord;
      obj['Object0' + counterForObject]['linestringCoord'] = linestringCoord;
      obj['Object0' + counterForObject]['linearRingCoord'] = linearRingCoord;
    } else {
      Math.round(Math.random()) ? console.log('Filtering bad data in progress .....') : console.log('Filtering bad data in progress .........')
    }
    //====================================================================

    //для оптимизации
    if(counterForObject % 100 == 0){
      fileNameCounter++;
      fs.writeFileSync('result_of_parsing_kml/' + fileNameCounter + '_result.json', JSON.stringify(obj));
      counterForObject = 0;
      obj = {};
      //return false;
    }
    //обнуляем временную переменую для следующего Placemark
    tmpStringOfCurrentTag = '';
  }
  //запись всех строчек между <Placemark></Placemark>
  if(isCopyAllObject){ tmpStringOfCurrentTag += line; }
  //если строка последняя записываем остатки в файл
  if(last){
    fileNameCounter++;
    fs.writeFileSync('result_of_parsing_kml/' + fileNameCounter + '_result.json', JSON.stringify(obj));
    obj = {};
    console.log('[KMLparser]: File (' + kmlFileName + ') succesfully read. Created (' + fileNameCounter + ') files like n_result.json');
    console.log('[KMLparser]: Time of parsing - ' + ((new Date() - scriptStartDate)/60000) + ' min.');
    //console.log('[KMLparser]: Debug counter of useless info: ' + debugCounter);
  }
});
